var manager_url = "http://127.0.0.1:8000"
function on_button_click() {
  document.getElementById("content").innerHTML = "Loading results…";
  document.getElementById("integration").innerHTML = "";
  var xmlhttp = new XMLHttpRequest();
  var searchedValue = document.getElementById("input").value;
  var url = manager_url + "/search?query=" + encodeURIComponent(searchedValue);

  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      values = JSON.parse(this.responseText);
      //Checks if values is a redirection
      if (values.redirection != null) {
        document.location.href = values.redirect;
        return;
      }
      if (values.integration != null) {
        //TODO integration title?
        document.getElementById("integration").innerHTML = values.integration.body;
        eval(values.integration.script);
      };
      if (values.results.length == 0) {
        document.getElementById("content").innerHTML = "No results."
      } else {
        document.getElementById("content").innerHTML = ""
        var total_content = ""
        values.results.forEach(page => {
          total_content += build_item(page);
        });
        document.getElementById("content").innerHTML = total_content;
      }
    };
  };
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}

function handle_statistics(a_tag) {
  if (document.getElementById("statistics").checked) {
    var xmlhttp = new XMLHttpRequest();
    var url = manager_url + "/confirm_visit?url=" + encodeURIComponent(a_tag.href);
    xmlhttp.open("GET", url, false); // Synchronous request
    xmlhttp.send();
    console.log("Opened GET to", url)
  }
}

function build_item(page) {
  var res = "<article>";
  res += "<div class='row'>";
  res += "<div class='col-1'><img src='"+page.favicon+"' width='16' height='16'></div>";
  res += "<div class='col'><h4><a onclick='handle_statistics(this)' href='"+page.url+"'>"+page.title+"</a></h4></div>";
  res += "</div><div class='row'>"
  res += "<p>"+page.description+"</p>";
  res += "<p class='url'>"+page.url+"</p></div></article>";
  return res
}

document.getElementById("button").onclick = on_button_click;
document.getElementById("input").addEventListener("keyup", ({key}) => {
  if (key === "Enter") {
    on_button_click();
  }
})
document.getElementById("statistics").onChange = console.log;
