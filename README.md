# Distributed Search

Par Kilian GUILLAUME et Raphaël CHARLES.

[[_TOC_]]

## Qu'est-ce que Distributed Search?

Distributed Search est un projet de moteur de recherche décentralisé. Il repose
sur un système de pairs sur lesquels il sera possible de récupérer des résultats
de recherche.

Distributed Search ne permet actuellement que de scanner les sites web normaux,
mais le support de tor et de Gemini est prévu dans le futur.

Distributed Search utilise l'API Reddit comme source d'origine pour les sites
web à parcourir.

## Architecture du projet

![Diagramme d'architecture du projet](architecture.png)

Distributed Search est composé de 3 parties :

- Un frontend, dans le dossier `frontend/`, présentant une interface graphique
depuis laquelle il est possible de lancer une recherche
- Le crawler, dans le dossier `crawler/`, qui parcours différent sites internet
afin de les ajouter dans la base de données de Distributed Search
- Le manager, mettant à disposition une API permettant au frontend et au crawler
de fonctionner

Distributed Search permet également d'effectuer des requêtes de manager à
manager afin de profiter des bases de données d'autres instances de Distributed
Search, et ainsi avoir de meilleurs résultats sans avoir à indéxer tout internet
sur un seul nœud du réseau.

## Comment est développé Distributed Search ?

Distributed Search est pour la partie backend réalisé en Python, avec notemment
FastAPI pour l'API exposée.

Le frontend quant à lui est réalisé en HTML+CSS et Javascript basique.

Nous avons tous les deux de l'expérience en Python, d'où le choix de cette technologie
comme langage de base pour le projet.

Pour ce qui est de FastAPI, Kilian avait déjà eu l'occasion de réaliser des
plateformes avec dans le cadre de son travail.

## Guide Hébergeur

- Configurer nginx ou apache pour servir de reverse-proxy au manager
(127.0.0.1:8000), et servir le frontend

- Editer le frontend (home.html) pour appeller le manager à la bonne addresse

- Démarrer le manager et le crawler:

```bash
docker-compose up --build -d
```

## Guide Utilisateur

### Recherche

Vous pouvez lancer une recherche simple en ouvrant la page d'accueil du frontend,
en tapant votre recherche dans l'entrée texte, puis en appuyant sur Entrée ou le
bouton rechercher.

### Integrations

Certaines recherches retourneront des intégrations en plus des résultats.

- `lorem ipsum`, `5 paragraphs of lorem ipsum`, … : Affiche des paragraphes de lorem ipsum
- `1 + 3`, `9 / 3 = ?`, … : Affiche le résultat du calcul
- `color picker` : Affiche un selecteur de couleurs

### Bangs

Il est possible de cherche des résultats directement sur un site donné grâce aux bangs.

- `wiki`, `wikipedia` : Affiche les résultats de la recherche sur Wikipedia (version anglaise)
- `wikifr`, `wiki_fr`, … : Affiche les résultats de la recherche sur Wikipedia (version française)
