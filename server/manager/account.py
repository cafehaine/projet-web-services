from typing import List, Optional

from fastapi import APIRouter, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from pydantic import BaseModel, Field

import server.storage as storage

account_api = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/account/login")


class UserLogin(BaseModel):
    username: str = Field(..., example="admin")


class UserFull(UserLogin):
    password: str = Field(..., example="admin")


@account_api.post("/login")
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    result = storage.login_user(form_data.username, form_data.password)
    if not result:
        print("Invalid username or password")
        raise HTTPException(status_code=400, detail="Incorrect username or password")
    print("Valid account")
    return {"access_token": result, "token_type": "bearer"}


def get_current_user(token: str = Depends(oauth2_scheme)) -> int:
    user = storage.get_user_by_token(token)
    if user is None:
        raise Exception("invalid token")  # TODO 401
    return user


@account_api.post("/register", response_model=None)
def register_user(
    user: UserFull, current_user: int = Depends(get_current_user)
) -> None:
    """Register a new user."""
    storage.register_user(user.username, user.password)


@account_api.put("/update_password", response_model=None)
def change_password(
    user: UserFull, current_user: int = Depends(get_current_user)
) -> None:
    """Change the password for the given username."""
    storage.change_password(user.username, user.password)


@account_api.delete("/", response_model=None)
def delete_account(
    user: UserLogin, current_user: int = Depends(get_current_user)
) -> None:
    """Delete the user for the given username."""
    storage.delete_account(user.username)
