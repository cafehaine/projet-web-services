import datetime
import re
from threading import Thread
import time
import traceback
from typing import Any, Dict, Tuple
from urllib.parse import urljoin

from typing import List, Optional, Union
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel, Field
import requests

from server.bangs import *
from server.bangs import BANGS
from server.integrations import *
from server.integrations import INTEGRATIONS
import server.storage as storage
from server.sources import *
from server.sources import SOURCES
from .account import account_api
from .admin import admin_api
from .crawling import crawling_api, put_to_crawl


class Integration(BaseModel):
    title: str = Field(..., example="Calculator")
    body: str = Field(..., example="<p>Hello world :)</p>")
    script: str = Field(..., example="alert('Hello!');")


class SearchResult(BaseModel):
    title: str = Field(..., example="Homepage | CaféHaine")
    description: str = Field(..., example="CaféHaine's personal blog")
    url: str = Field(..., example="https://cafehaine.eu/")
    favicon: Optional[str] = Field(..., example="https://cafehaine.eu/favicon.png")


class SearchResponse(BaseModel):
    redirection: Optional[str] = Field(..., example="https://cafehaine.eu/")
    integration: Optional[Integration]
    results: List[SearchResult]


app = FastAPI()
app.include_router(account_api, prefix="/account", tags=["account"])
app.include_router(admin_api, prefix="/admin", tags=["admin"])
app.include_router(crawling_api, prefix="/crawling", tags=["crawling"])

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

CACHE_DURATION = datetime.timedelta(
    seconds=30
)  # quite big, for debugging, too small for production..
RESULT_CACHE: Dict[str, Tuple[SearchResponse, datetime.datetime]] = {}
FLAT_RESULT_CACHE: Dict[str, Tuple[List[SearchResult], datetime.datetime]] = {}


def _peer_search(query: str) -> List[SearchResult]:
    print("Fetching results from peers")
    output = []
    for peer in storage.list_peers():
        print(f"Fetching from {peer.url!r}")
        try:
            result = requests.get(
                urljoin(peer.url, "flat_search"), params={"query": query}
            )
            if result.ok:
                results = [SearchResult.parse_obj(item) for item in result.json()]
                print(f"Got {len(results)} results from {peer.url!r}.")
                output += results
            else:
                print(f"Peer returned an error:")
                print(result.json())
        except requests.RequestException:
            print(f"Could not fetch results from {peer.url!r}:")
            traceback.print_exc()

    return output


def _get_integration(query: str) -> Optional[Integration]:
    print("Query:", query)
    if query in INTEGRATIONS:
        return INTEGRATIONS[query](query)
    for key, value in INTEGRATIONS.items():
        if isinstance(key, re.Pattern):
            if key.match(query):
                result = value(query)
                if result is not None:
                    return Integration(title="TODO", body=result[0], script=result[1])
    return None


def _get_bang(query: str) -> Optional[str]:
    if not query.startswith("!"):
        return None

    keyword, args = query.split(" ", maxsplit=1)
    keyword = keyword.removeprefix("!").lower()
    if keyword in BANGS:
        return BANGS[keyword](args)
    return None


@app.get("/status")
def status() -> str:
    return "OK"


@app.get("/search", response_model=SearchResponse, tags=["search"])
def search(query: str) -> SearchResponse:
    if query in RESULT_CACHE:
        response, timestamp = RESULT_CACHE[query]
        if datetime.datetime.now() - timestamp < CACHE_DURATION:
            print(f"Answering to {query!r} using a cached response")
            return response

    bang = _get_bang(query)

    if bang is not None:
        response = SearchResponse(redirection=bang, integration=None, results=[])
        RESULT_CACHE[query] = (response, datetime.datetime.now())
        return response

    flat_result = flat_search(query)
    peer_result = _peer_search(query)
    integration = _get_integration(query)

    output = flat_result + peer_result
    response = SearchResponse(redirection=None, integration=integration, results=output)

    RESULT_CACHE[query] = (response, datetime.datetime.now())
    return response


@app.get("/flat_search", response_model=List[SearchResult], tags=["search"])
def flat_search(query: str) -> List[SearchResult]:
    if query in FLAT_RESULT_CACHE:
        response, timestamp = FLAT_RESULT_CACHE[query]
        if datetime.datetime.now() - timestamp < CACHE_DURATION:
            print(f"Answering to {query!r} using a cached response")
            return response

    output = [
        SearchResult(
            title=result.title,
            url=result.url,
            description=result.description,
            favicon=result.favicon,
        )
        for result in storage.search(query)
    ]
    FLAT_RESULT_CACHE[query] = (output, datetime.datetime.now())
    return output


@app.get("/confirm_visit", response_model=None, tags=["search"])
def confirm_visit(url: str) -> None:
    storage.confirm_visit(url)


# PERIODIC MAINTAINANCE
def _call_sources():
    for source in SOURCES:
        for url in source():
            storage.put_to_crawl([url])


@app.on_event("startup")
def start_sources_thread() -> None:
    thread = Thread(daemon=True)

    def thread_loop():
        while True:
            _call_sources()
            time.sleep(60 * 60 * 24)  # Every 24h

    thread.run = thread_loop
    thread.start()
