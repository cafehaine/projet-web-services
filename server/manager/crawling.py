from typing import Dict, List, Optional

from fastapi import APIRouter, Depends
from pydantic import BaseModel, Field

import server.storage as storage


def check_crawler_token(key: str) -> None:
    if not storage.crawler_exists(key):
        raise Exception("invalid token")


crawling_api = APIRouter(dependencies=[Depends(check_crawler_token)])


class Page(BaseModel):
    url: str = Field(..., example="https://cafehaine.eu/")
    title: str = Field(..., example="Homepage | CaféHaine")
    desc: str = Field(..., example="CaféHaine's personal blog")
    content: str = Field(..., example="Some text that is on the page…")
    favicon: Optional[str] = Field(..., example="https://cafehaine.eu/favicon.png")


class URLSet(BaseModel):
    urls: List[str]


@crawling_api.get("/to_crawl", response_model=List[str])
def get_to_crawl() -> List[str]:
    # TODO authentication (maybe not?)
    return storage.get_to_crawl()


@crawling_api.post("/to_crawl", response_model=None)
def put_to_crawl(urls: URLSet) -> None:
    # TODO authentication
    storage.put_to_crawl(urls.urls)


@crawling_api.delete("/to_crawl", response_model=None)
def delete_from_crawl(urls: URLSet) -> None:
    # TODO authentication
    for url in urls.urls:
        storage.delete_from_crawl(url)


@crawling_api.post("/pages", response_model=None)
def put_page(page: Page) -> None:
    # TODO authentication
    print(page)
    storage.delete_from_crawl(page.url)
    storage.put_page(page.title, page.desc, page.url, page.content, page.favicon)
