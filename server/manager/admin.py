from typing import List, Optional, Tuple

from fastapi import APIRouter, Depends
from pydantic import BaseModel, Field

import server.storage as storage
from .account import get_current_user

admin_api = APIRouter(dependencies=[Depends(get_current_user)])


class PeerUrl(BaseModel):
    url: str = Field(..., example="https://dse.cafehaine.eu/")


class PeerFull(PeerUrl):
    comment: Optional[str] = Field(None, example="CaféHaine's own instance.")


@admin_api.get(
    "/crawlers",
    response_model=List[Tuple[int, str]],
    summary="List the registered crawlers.",
    tags=["crawlers"],
)
def get_crawlers() -> List[Tuple[int, str]]:
    """List this server's registered crawlers."""
    return storage.list_crawlers()


@admin_api.post(
    "/crawlers", response_model=str, summary="Register a crawler.", tags=["crawlers"]
)
def post_crawlers(comment: Optional[str]) -> str:
    """Register a crawler and return it's token."""
    return storage.register_crawler(comment)


@admin_api.delete(
    "/crawlers", response_model=None, summary="Delete a crawler.", tags=["crawlers"]
)
def delete_crawler(id: int) -> None:
    """Delete a crawler."""
    storage.delete_crawler(id)


@admin_api.put(
    "/crawlers",
    response_model=None,
    summary="Update a crawler's comment.",
    tags=["crawlers"],
)
def put_crawler(id: int, comment: Optional[str]) -> str:
    """Update a crawler's comment given it's id."""
    storage.update_crawler_comment(id, comment)


@admin_api.get(
    "/peers",
    response_model=List[PeerFull],
    summary="List this instance's peers.",
    tags=["peers"],
)
def get_peers() -> List[PeerFull]:
    """
    List this server's peers.

    Those peers will be called whenever a non-flat search query is executed.
    """
    # TODO authentication on admin routes
    return [
        PeerFull(url=storage_peer.url, comment=storage_peer.comment)
        for storage_peer in storage.list_peers()
    ]


@admin_api.put(
    "/peers", response_model=None, summary="Register or update a peer.", tags=["peers"]
)
def put_peers(peer: PeerFull) -> None:
    """Register a new peer."""
    # TODO authentication on admin routes
    storage.register_peer(storage.Peer(url=peer.url, comment=peer.comment))


@admin_api.delete(
    "/peers", response_model=None, summary="Remove a peer.", tags=["peers"]
)
def delete_peers(peer_url: PeerUrl) -> None:
    """Remove a peer given it's url."""
    # TODO authentication on admin routes
    storage.delete_peer(peer_url.url)
