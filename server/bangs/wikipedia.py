from urllib.parse import quote_plus

import requests

from server.constants import HEADERS
from server.bangs import register_bang


@register_bang("wikifr", "wiki_fr", "wikipediafr", "wikipedia_fr")
def wiki_french(query: str) -> str:
    quoted = quote_plus(query)
    results = requests.get(
        f"https://fr.wikipedia.org/w/api.php?action=opensearch&search={quoted}&namespace=0",
        headers=HEADERS,
    ).json()
    return results[3][0]


@register_bang("wiki", "wikipedia")
def wiki_english(query: str) -> str:
    quoted = quote_plus(query)
    results = requests.get(
        f"https://en.wikipedia.org/w/api.php?action=opensearch&search={quoted}&namespace=0",
        headers=HEADERS,
    ).json()
    return results[3][0]
