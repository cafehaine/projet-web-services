__all__ = ["wikipedia"]

BANGS = {}


def register_bang(*names):
    def wrapper(func):
        for name in names:
            BANGS[name] = func
        return func

    return wrapper
