from datetime import datetime

from pony.orm import *

DB = Database()


TOKEN_BYTES = 256


class ToCrawl(DB.Entity):
    url = PrimaryKey(str)
    timestamp = Required(datetime)


class Page(DB.Entity):
    url = PrimaryKey(str)
    title = Required(str)
    desc = Required(str)
    content = Required(str)
    favicon = Optional(str)
    visits = Required(int)


class Peer(DB.Entity):
    url = PrimaryKey(str)
    comment = Optional(str)


class Admin(DB.Entity):
    username = Required(str, unique=True)
    password = Optional(str)  # TODO compute based on length of salt+password
    auth_tokens = Set("Token")


class Token(DB.Entity):
    user = Required(Admin)
    token = Required(str, max_len=TOKEN_BYTES, unique=True)


class Crawler(DB.Entity):
    token = Required(str, max_len=TOKEN_BYTES, unique=True)
    comment = Optional(str)
