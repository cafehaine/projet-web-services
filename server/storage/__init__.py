from datetime import datetime
from enum import Flag, auto
import json
import os
import secrets
from typing import Any, Dict, List, NamedTuple, Tuple, Optional

from pony.orm import db_session, exists

from .models import DB, ToCrawl, Page, Crawler, Token, Admin, TOKEN_BYTES
from .models import Peer as PeerDB


class SearchResult(NamedTuple):
    title: str
    description: str
    url: str
    favicon: Optional[str]


class Peer(NamedTuple):
    url: str
    comment: str


def login_user(username: str, password: str) -> Optional[str]:
    """Try to login a user, and return a token or None."""
    with db_session:
        admin = Admin.get(username=username)
        if admin is None:
            return None
        # TODO check password with salt/hash function
        token = secrets.token_urlsafe(int(TOKEN_BYTES / 8 * 6) - 1)
        Token(user=admin, token=token)
        return token


def get_user_by_token(token: str) -> Optional[int]:
    """Find a user given a token, and return it's id if the token is valid."""
    with db_session:
        db_token = Token.get(token=token)
        if db_token is None:
            return None
        return db_token.user.id


def register_user(username: str, password: str) -> None:
    """Register a new user."""
    with db_session:
        admin = Admin(username=username, password=password)  # TODO salt + hash


def change_password(username: str, password: str) -> None:
    """Change the password for the given username."""
    with db_session:
        admin = Admin.get(username=username)
        admin.password = password  # TODO salt + hash


def delete_account(username: str) -> None:
    """Delete the user for the given username."""
    with db_session:
        admin = Admin.get(username=username)
        if admin is not None:
            admin.delete()


def register_crawler(comment: Optional[str] = None) -> str:
    """Register a new crawler and return it's token."""
    with db_session:
        token = secrets.token_urlsafe(int(TOKEN_BYTES / 8 * 6) - 1)
        Crawler(token=token, comment=comment)
        return token


def update_crawler_comment(id: int, comment: Optional[str] = None) -> None:
    """Change the comment for a crawler."""
    with db_session:
        crawler = Crawler.get(id)
        if crawler is not None:
            crawler.comment = comment


def crawler_exists(token: str) -> bool:
    """Check if a token is valid."""
    with db_session:
        crawler = Crawler.get(token=token)
        return crawler is not None


def list_crawlers() -> List[Tuple[int, str]]:
    """Return a list of the current crawler ids, and their comment."""
    with db_session:
        return [(crawler.id, crawler.comment) for crawler in Crawler.select()]


def delete_crawler(id: int) -> None:
    """Delete a crawler."""
    with db_session:
        crawler = Crawler.get(id=id)
        if crawler is not None:
            crawler.delete()


def list_peers() -> List[Peer]:
    """Return peers for distributed search."""
    with db_session:
        return [Peer(peer.url, peer.comment) for peer in PeerDB.select()]


def register_peer(peer: Peer) -> None:
    """Add a peer to the peer list."""
    with db_session:
        db_peer = PeerDB.get(url=peer.url)
        if db_peer is None:
            db_peer = PeerDB(url=peer.url, comment=peer.comment)
        else:
            db_peer.comment = peer.comment


def delete_peer(peer_url: str) -> None:
    """Remove a peer from the peer list."""
    with db_session:
        db_peer = PeerDB.get(url=peer_url)
        if db_peer is not None:
            db_peer.delete()


def search(query: str) -> List[SearchResult]:
    query = query.lower()
    res = []
    with db_session:
        for page in Page.select(lambda p: query in p.desc or query in p.content):
            res.append(
                SearchResult(
                    page.title,
                    page.desc,
                    page.url,
                    page.favicon,
                )
            )
    return res


def get_to_crawl() -> List[str]:
    with db_session:
        pages = ToCrawl.select().order_by(ToCrawl.timestamp)[:10]
        return [page.url for page in pages]


def put_to_crawl(urls: List[str]) -> None:
    with db_session:
        for url in urls:
            page = ToCrawl.get(url=url)
            if page is None:
                ToCrawl(url=url, timestamp=datetime.now())


def delete_from_crawl(url: str) -> None:
    """Remove an url from the to_crawl list."""
    with db_session:
        db_url = ToCrawl.get(url=url)
        if db_url is not None:
            db_url.delete()


def put_page(title: str, desc: str, url: str, content: str, favicon: str) -> None:
    content = content.lower()
    with db_session:
        to_crawl = ToCrawl.get(url=url)
        if to_crawl is not None:
            to_crawl.delete()
        page = Page.get(url=url)
        if page is None:
            page = Page(
                url=url,
                title=title,
                desc=desc,
                content=content,
                favicon=favicon,
                visits=0,
            )
        else:
            page.title = title
            page.desc = desc
            page.content = content
            page.favicon = favicon


def confirm_visit(url: str) -> None:
    """Increment the visits count for a given url."""
    with db_session:
        page = Page.get(url=url)
        if page is not None:
            page.visits += 1


def init_db():
    print("Initializing the database")
    db_path = os.getenv("DS_DB_PATH", "database.sqlite")
    DB.bind(provider="sqlite", filename=db_path, create_db=True)
    DB.generate_mapping(create_tables=True)
    with db_session:
        if Admin.get(username="root") is None:
            print("Created default root:root admin.")
            Admin(username="root", password="root")  # TODO salt+hash
        if not exists(c for c in Crawler):
            tokens = os.getenv("INITIAL_CRAWLER_TOKENS", None)
            if tokens is None:
                print("DID NOT REGISTER ANY CRAWLER.")
            else:
                for token in tokens.split(";"):
                    token = token.strip()
                    print(f"Registering a crawler with the token {token!r}.")
                    Crawler(token=token, comment="Initial crawler")


init_db()


# FOR TESTING ONLY


def main():
    put_page(
        "Test",
        "blabla",
        "https://cafehaine.eu",
        "anuristenratuirsaturs",
        "https://cafehaine.eu/favicon.png",
    )
    put_to_crawl("https://cafehaine.eu")
    print(get_to_crawl())
    print(search("anur"))


if __name__ == "__main__":
    main()
