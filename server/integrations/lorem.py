import re
from typing import Optional, Tuple

from server.integrations import register_integration

RE_LOREM = re.compile(
    r"(?:(?P<paragraphs>\d+)\s+paragraphs?(?:\s+of)?\s+)?lorem\s+ipsum"
)

INTEGRATION_BODY = """
<div>
    <div id="integration-title"></div>
    <div id="integration-content"></div>
    <div id="integration-buttons"></div>
</div>
"""

INTEGRATION_SCRIPT = """
function buildLorem(nbPars) {
    document.getElementById("integration-title").innerHTML = "<h4 class='title'>" + nbPars + " paragraphs of Lorem Ipsum</h4>";
    document.getElementById("integration-content").innerHTML = loremIpsum(nbPars);
    document.getElementById("integration-buttons").innerHTML = loremButtons();

    document.getElementById("lorem-button-1").onclick = () => buildLorem(1);
    document.getElementById("lorem-button-3").onclick = () => buildLorem(3);
    document.getElementById("lorem-button-5").onclick = () => buildLorem(5);
};

function loremIpsum(nbPars) {
    var paragraphs = [
        "<p>Dolores mollitia quo temporibus non alias nobis autem. Ipsam aut ab non sunt incidunt et atque. Deleniti aliquid distinctio sed expedita veniam quia sunt et. Quia qui vitae rerum dolore excepturi qui voluptatibus eum. Tempore laboriosam vero et.</p>",
        "<p>Recusandae non maxime rem voluptas odio esse ut eum. Id id itaque atque possimus deserunt nihil sit. Est optio iure quis eligendi eum eum. Laborum suscipit consectetur expedita.</p>",
        "<p>Facilis quaerat sint consequuntur sed. Autem labore sunt et aut consequatur cumque. Ut ab et perferendis. Ipsam nemo et perspiciatis voluptatibus maiores. Aut sint ullam asperiores voluptatem quis.</p>",
        "<p>Aperiam nobis impedit quia et sequi odit voluptatem. Suscipit est sed hic autem ducimus. Voluptas accusamus sed aspernatur facilis fuga. Eaque laboriosam ipsam numquam doloribus doloremque. Voluptatibus voluptatibus fugiat aspernatur eius delectus sint temporibus. Deserunt in omnis officia.</p>",
        "<p>Et architecto excepturi corporis autem. Voluptatem illum nostrum facilis. Facilis nostrum qui alias ex quia facilis dolor. Quia quia est et. Quasi debitis quos natus.</p>",
    ];
    var res = "";

    for (let i = 0; i < nbPars; i++) {
        res += paragraphs[i];
    };
    return res;
};

function loremButtons() {
    var nbParagraphs = [1,3,5];
    var i;
    var res = "<div class='row'>";
    res += "<h5 class='integration-buttons-title'>Choix du nombre de paragraphes</h5>";
    res += "</div>";
    res += "<div class='row'>";
    for (i=0;i<3;i++) {
        res += "<div class='col'>";
        res += "<button id='lorem-button-" + nbParagraphs[i] + "'>" + nbParagraphs[i] + "</button>"
        res += "</div>";
    }
    res += "</div>";

    return res;
};

buildLorem({DEFAULT_COUNT});
"""


@register_integration(RE_LOREM)
def lorem(query: str) -> Optional[Tuple[str, str]]:
    match = RE_LOREM.match(query)
    if match is None:
        return None
    par_count = int(match.group("paragraphs") or 3)
    par_count = min(max(par_count, 1), 5)
    return INTEGRATION_BODY, INTEGRATION_SCRIPT.replace(
        "{DEFAULT_COUNT}", str(par_count)
    )
