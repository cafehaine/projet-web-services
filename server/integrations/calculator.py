import re
from typing import Optional, Tuple

from server.integrations import register_integration

RE_EXPRESSION = re.compile(
    r"^\s*(?:(?:\d+\.\d*|\d*\.\d+|\d+)\s*[+*/-]\s*)*(?:\d+\.\d*|\d*\.\d+|\d+)(?:\s*=(?:\s*\?)?)?\s*$"
)

INTEGRATION_BODY = (
    """<div id="formula">{FORMULA}</div>=<div id="result">{RESULT}</div>"""
)

INTEGRATION_SCRIPT = """"""


@register_integration(RE_EXPRESSION)
def calc(query: str) -> Optional[Tuple[str, str]]:
    match = RE_EXPRESSION.match(query)
    if match is None:
        return None
    expr = match[0]
    expr = expr.rstrip("?= \t\n\r")
    print(f"Query: {query!r}, expr: {expr!r}")
    result = eval(expr)
    return (
        INTEGRATION_BODY.replace("{RESULT}", str(result)).replace("{FORMULA}", expr),
        INTEGRATION_SCRIPT,
    )
