import re
from typing import Optional, Tuple

from server.integrations import register_integration

RE_COLORPICKER = re.compile(r"color picker")

INTEGRATION_BODY = """
    <div class="row">
        <div class="col-4">
            <label for="colorpicker">Color Picker:</label>
        </div>
        <div class="col-4">
            <input type="color" id="colorpicker" value="#0000ff">
        </div>
        <div class="col-4">
            <p id="colorcode">
                #0000ff
            </p>
        </div>
    </div>
"""

INTEGRATION_SCRIPT = """
    var colorpicker
    var defaultColor = "#0000ff";

    function updateColorCode(event) {
        document.getElementById("colorcode").innerHTML = event.target.value
    }
    
    function startup() {
        colorpicker = document.querySelector("#colorpicker");
        colorpicker.value = defaultColor;
        colorpicker.addEventListener("change", updateColorCode, false);
        colorpicker.select();
    }

    startup();
"""


@register_integration(RE_COLORPICKER)
def colorpicker(query: str) -> Optional[Tuple[str, str]]:
    match = RE_COLORPICKER.match(query)
    if match is None:
        return None
    return INTEGRATION_BODY, INTEGRATION_SCRIPT
