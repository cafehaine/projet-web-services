__all__ = ["lorem", "calculator", "colorpicker"]

INTEGRATIONS = {}


def register_integration(*names):
    def wrapper(func):
        for name in names:
            INTEGRATIONS[name] = func
        return func

    return wrapper
