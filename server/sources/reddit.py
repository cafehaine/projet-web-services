from typing import List
import requests

from time import sleep

from server.constants import HEADERS
from server.sources import register_source


@register_source
def reddit_source() -> List[str]:
    # TODO fetch the reddit API to extract links directly
    url = "https://old.reddit.com/r/InternetIsBeautiful.json"
    res = []
    request = requests.get(url, headers=HEADERS).json()
    while "error" in request:
        sleep(10)
        request = requests.get(url, headers=HEADERS).json()
    for child in request["data"]["children"]:
        if "url" in child.get("data"):
            res.append(child["data"]["url"])
    print("found {} links".format(len(res)))
    return res
