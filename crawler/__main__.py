from argparse import ArgumentParser
import http
import logging
import os
import sys
from typing import Dict, Optional, NamedTuple, List, Tuple, Set
import time
import urllib.error
from urllib.parse import urlparse, urljoin
from urllib.robotparser import RobotFileParser

import requests
from bs4 import BeautifulSoup

SERVER_URL = os.getenv("DS_SERVER_URL", "http://127.0.0.1:8000")
TOKEN = os.getenv("TOKEN")
SLEEP_TIME = 5

EXCLUDE_LIST_URL_PREFIX = (
    "javascript:",
    "tel:",
    "mailto:",
    "geo:",
)  # TODO switch to an include list?

ROBOTS_CACHE: Dict[str, RobotFileParser] = {}  # TODO expire after 1h?


class ExtractedPage(NamedTuple):
    title: str
    url: str
    description: str
    page_content: str
    favicon: str

    def to_dict(self) -> Dict[str, str]:
        return {
            "title": self.title,
            "url": self.url,
            "desc": self.description,
            "content": self.page_content,
            "favicon": self.favicon,
        }


def fetch_urls_to_crawl() -> List[str]:
    """Fetch urls to crawl from the server."""
    res = requests.get(f"{SERVER_URL}/crawling/to_crawl", params={"key": TOKEN})
    return res.json()


def can_fetch(url: str) -> bool:
    """
    Check using the robots.txt if this url can be crawled.

    Checks all levels of a site before returning a result:
    For a given url https://cafehaine.eu/articles/The-art-of-multi-threading/:
    https://cafehaine.eu/articles/The-art-of-multi-threading/robots.txt
    https://cafehaine.eu/articles/robots.txt
    https://cafehaine.eu/robots.txt
    """
    to_check = "robots.txt"
    last_checked = None
    logging.info("Checking robots.txt for %r", url)
    while urljoin(url, to_check) != last_checked:
        current = urljoin(url, to_check)
        last_checked = current
        if current not in ROBOTS_CACHE:
            logging.debug("Fetching robots.txt at %r.", current)
            ROBOTS_CACHE[current] = RobotFileParser(current)
            try:
                result = requests.get(current, allow_redirects=True, timeout=5)
                ROBOTS_CACHE[current].parse(result.text.split("\n"))
            except requests.RequestException:
                logging.debug("Errored out, probably not allowed.")
                return False
        if not ROBOTS_CACHE[current].can_fetch(
            "USERAGENT", url
        ):  # TODO real user agent
            logging.debug("Not allowed.")
            return False
        to_check = "../" + to_check
    logging.debug("Allowed by robots.txt.")
    return True


def invalid_url(url: str, message: str) -> None:
    logging.warning("Cannot fetch %r: %s.", url, message)
    result = requests.delete(
        f"{SERVER_URL}/crawling/to_crawl", json={"urls": [url]}, params={"key": TOKEN}
    )
    if not result.ok:
        logging.error(
            "Remote error when deleting url to crawl: %d.", result.status_code
        )
        logging.info("%s", result.json())


def find_favicon(url: str, soup: BeautifulSoup) -> Optional[str]:
    favicon = (
        soup.find("link", rel="shortcut icon")
        or soup.find("link", rel="icon")
        or soup.find("link", rel="apple-touch-icon")
        or soup.find("link", rel="apple-touch-icon-precomposed")
    )
    if favicon:
        favicon_url = urljoin(url, favicon["href"])
    else:
        favicon_url = (
            "https://www.google.com/s2/favicons?domain=" + urlparse(url).netloc
        )
    return favicon_url


def crawl_url(url: str) -> Optional[Tuple[ExtractedPage, Set[str]]]:
    """Crawl a given url."""
    if not can_fetch(url):
        invalid_url(url, "blocked by robots.txt")
        return None
    # TODO define a user agent for requests (use a custom session?)
    # TODO send a HEAD first to check the content type?
    result: requests.Response
    try:
        logging.debug("Fetching head")
        result = requests.head(url, allow_redirects=True, timeout=5)
        if not result.ok:
            invalid_url(url, "error when fetching head")
            return None
        content_type = result.headers.get("content-type", "").lower()
        if not content_type.startswith("text/html"):
            invalid_url(url, f"content type is not html: {content_type}.")
            return None
        logging.debug("Fetching body")
        result = requests.get(url)
        if not result.ok:
            invalid_url(url, "error when fetching page")
            return None
    except requests.exceptions.RequestException as exc:
        invalid_url(url, f"could not fetch page: {exc}.")
        return None

    bs = BeautifulSoup(result.text, features="lxml")
    description = bs.find("meta", property="og:description", content=True)
    if description is None:
        # Used on reddit
        description = bs.find(
            lambda tag: tag.name == "meta"
            and tag.get("name", "") == "description"
            and hasattr(tag, "content")
        )

    if description:
        description_text = description["content"]
    if description is None or not description_text:
        description_text = "Unable to extract description"

    links = bs.find_all("a", href=True)
    linkset = set()
    for link in links:
        actual_link: str = link["href"]
        if "#" in actual_link:
            actual_link, _ = actual_link.split("#", maxsplit=1)
        if actual_link.startswith(EXCLUDE_LIST_URL_PREFIX):
            continue
        linkset.add(urljoin(url, actual_link))

    title = bs.find("title")
    if title is None:
        invalid_url(url, "no title on page")
        return None

    body = bs.find("body")
    if body is None:
        invalid_url(url, "no body tag")
        return None

    content = body.get_text(strip=True)
    if not content:
        # Some pages are completely empty
        invalid_url(url, "empty bage body")
        return None

    favicon_url = find_favicon(url, bs)

    epage = ExtractedPage(
        title.get_text(),
        url,
        description_text,
        content,
        favicon_url,
    )
    return epage, linkset


def push_extracted_page(page: ExtractedPage) -> None:
    """Send an extracted page to the server."""
    result = requests.post(
        f"{SERVER_URL}/crawling/pages", json=page.to_dict(), params={"key": TOKEN}
    )
    if not result.ok:
        logging.error(
            "Remote error when pushing extracted page: %d.", result.status_code
        )
        logging.info("%s", result.json())


def push_extracted_urls(urls: List[str]) -> None:
    """Send the extracted urls."""
    result = requests.post(
        f"{SERVER_URL}/crawling/to_crawl", json={"urls": urls}, params={"key": TOKEN}
    )
    if not result.ok:
        logging.error(
            "Remote error when pushing extracted urls: %d.", result.status_code
        )
        logging.info("%s", result.json())


def main():
    logging.info("Waiting for the server")
    ok = False
    while not ok:
        try:
            result = requests.get(f"{SERVER_URL}/status")
            ok = result.text == '"OK"'
        except Exception:
            time.sleep(SLEEP_TIME)
    logging.info("Server is ready")
    while True:
        urls = fetch_urls_to_crawl()
        logging.info("Crawling urls:")
        logging.info("%s", urls)
        for url in urls:
            time.sleep(SLEEP_TIME)
            logging.info("Crawling %r.", url)
            result = crawl_url(url)
            if result is None:
                continue
            extracted, found_urls = result
            push_extracted_page(extracted)
            if found_urls:
                push_extracted_urls(list(found_urls))


if __name__ == "__main__":
    log_level = os.getenv("LOG_LEVEL", "WARN")
    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % log_level)
    logging.basicConfig(level=numeric_level)

    sys.stdout.reconfigure(line_buffering=True)
    logging.debug("Distributed Search server url is %s", SERVER_URL)
    main()
